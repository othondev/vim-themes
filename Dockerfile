FROM alpine:latest
ARG script_url
ARG id_group=1000
ARG id_user=1000

RUN apk add --no-cache \
  vim \
  curl \
  git

RUN addgroup -g $id_group -S user && \
    adduser -u $id_user -S user -G user

USER user

RUN curl -s $script_url | sh

WORKDIR /home/user/workspace

ENTRYPOINT ["/usr/bin/vim"]
