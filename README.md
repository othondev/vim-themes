# vim-themes

## How to use
### Build image
```bash
docker build --build-arg script_url=https://raw.githubusercontent.com/othondev/dot-vim/master/install.sh .
```
### Some images already to use
```bash
docker run --rm -it -v $PWD:/home/user/workspace othondev/vim-themes:lopes
```
### Recomendations:
- You can use alias with your favorite themes
    ```bash
    alias edit="docker run --rm -it -v $PWD:/home/user/workspace othondev/vim-themes:lopes"
    ```
